import java.util.*;
public class D30Q90 {
    static int reverse;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number to be reversed: ");
        int a=sc.nextInt();
        int r=rev(a);
        System.out.println(r);
    }
    public static int rev(int a){
        if(a!=0){
            int rem=a%10;
            reverse=reverse*10+rem;
            rev(a/10);
        }
        return reverse;
    }
}
