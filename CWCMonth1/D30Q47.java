import java.util.*;
public class D30Q47 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your number: ");
        int a=sc.nextInt();
        int r=0, arm=0; int ori=a;
        while(a>0) {
        	r=a%10;
        	arm=(int) (arm+ Math.pow(r, 3));
        	a=a/10;
        }
        if(arm==ori) System.out.println("Armstrong number");
        else System.out.println("Not an armstrong number");
    }
}
