import java.util.*;
public class D30Q35 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number: ");
        int a=sc.nextInt();
        //int f=fibo(a);
        System.out.println("Nth fibonacci term is: "+fibo(a));
    }
    public static int fibo(int a){
        if(a==1) return 0;
        else if(a==2) return 1;
        else return fibo(a-1)+fibo(a-2);
    }
}
