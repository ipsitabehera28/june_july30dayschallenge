import java.util.*;
public class D30Q31 {
	static int s;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        //System.out.println("Enter the order of matrix: ");
        int [][] a= new int[4][4];
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                System.out.println("Enter your element: "+ i +" and "+j);
                a[i][j]=sc.nextInt();
            }
        }
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
                System.out.println(a[i][j]+"\t");  
            }
            System.out.println();
        }
        for(int i=0;i<4;i++){
            for(int j=0;j<4;j++){
            	if(i==0) 
            		s=s+a[i][j];
            	else if(i==3)
            		s=s+a[i][j];
            	else if(j==3)
            		s=s+a[i][j];
            	else if(j==3)
            		s=s+a[i][j];
            }
        }
        System.out.println("Sum of border elements: "+s);
    }
}
