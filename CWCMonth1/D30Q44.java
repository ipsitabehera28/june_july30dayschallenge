import java.util.*;
public class D30Q44 {
	static int r;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter your number: ");
        int a=sc.nextInt();
        int bin=tobinary(a);
        int rev=reverse(bin);
        if(bin==rev) System.out.println("Binary value of "+a+ " is Palindrome");
        else System.out.println("Binary value of "+a+" is not Palindrome");
    }
    public static int tobinary(int a) {
    	if(a<=1) return 1;
		else return tobinary(a/2)*10+a%2;
    }
    public static int reverse(int bin) {
    	if(bin!=0) {
			int rem=bin%10;
			r=r*10+rem;
			reverse(bin/10);
		}
			return r;
    }
}

