import java.util.*;
public class D30Q24 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the number of elements in the array: ");
        int a=sc.nextInt();
        int arr[]=new int[a];
        System.out.println("Enter the elements: ");
        for(int i=0;i<a;i++){
            arr[i]=sc.nextInt();
        }
        System.out.println("Array: ");
        int j = -1,t;
        for(int i=0;i<a;i++){
        	if(arr[i]%2==0) {
        		j++;
        		t = arr[i];
                arr[i] = arr[j];
                arr[j] = t;
        	}
            //System.out.print(arr[i]+" ");
        }
        for(int i=0;i<a;i++){
            System.out.print(arr[i]+" ");
        }
        System.out.println();
    }
}
