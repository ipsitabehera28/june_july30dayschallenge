import java.util.*;
public class D30Q87 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Input a float number: ");
        float f=sc.nextFloat();
        float round=Math.round(f);
        System.out.print("The rounded value of ");
        System.out.printf("%.6f",f);
        System.out.printf(" is: "+"%.2f",round);
    }
}
