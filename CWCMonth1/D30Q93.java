import java.math.BigInteger;
public class D30Q93 {
    
    public static void main(String[] args) {
    	BigInteger a=new BigInteger("1");
    	BigInteger b=new BigInteger("0");
    	BigInteger c=new BigInteger("0");
        for(int i=1;i<=15;i++){
        	a=a.multiply(BigInteger.valueOf(2));
        }
        //System.out.println(a);
        BigInteger cout=BigInteger.ZERO;       
        while(a!=BigInteger.valueOf(0)){
            b=b.add(a.mod(BigInteger.valueOf(10)));
            //c=c.add(BigInteger.valueOf(b));
        	a=a.divide(BigInteger.valueOf(10));
        	
        }
        System.out.println("Sum of digits are = "+b);
    }
}
