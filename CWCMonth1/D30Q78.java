import java.util.*;
public class D30Q78 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the length of array: ");
        int a=sc.nextInt();
        int b[]=new int[a];
        System.out.println("Enter the elements: ");
        for(int i=0;i<a;i++){
            b[i]=sc.nextInt();
        }
        System.out.println("Array: ");
        for(int i=0;i<a;i++){
            System.out.print(b[i]+" ");
        }
        System.out.println();
        //System.out.println("Swapping");
        System.out.println("Enter the index number from the beginning to swap from the end: ");
        int c=sc.nextInt();
        int t=b[c];
        b[c]=b[a-c];
        b[a-c]=t;
        System.out.println("After swapping:");
        for(int i=0;i<a;i++){
            System.out.print(b[i]+" ");
        }
        System.out.println();

        
    }
}
