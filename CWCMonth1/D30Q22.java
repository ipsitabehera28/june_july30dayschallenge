import java.util.*;

public class D30Q22 {
    public static int multiply(int a,int b) {
        int result=0;
        while(b>0) {
            if(b%2!=0) {
                result+=a;
            }
            a=a<<1;
            b=b>>1;
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
		System.out.println("Enter two number to multiply");
		int a=sc.nextInt();
		int b=sc.nextInt();
		System.out.println(a + "X" + b + " = " + multiply(a, b));
    }
}
