import java.util.*;
public class D30Q4 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter coordinates of four vertices: ");
        System.out.println("Enter coordinate of vertex A(first x then y): ");
        double a1=sc.nextDouble();
        double a2=sc.nextDouble();
        System.out.println("Enter coordinate of vertex B(first x then y): ");
        double b1=sc.nextDouble();
        double b2=sc.nextDouble();
        System.out.println("Enter coordinate of vertex C(first x then y): ");
        double c1=sc.nextDouble();
        double c2=sc.nextDouble();
        System.out.println("Enter coordinate of vertex D(first x then y): ");
        double d1=sc.nextDouble();
        double d2=sc.nextDouble();
        double ab=distance(a1,a2,b1,b2);
        double bc=distance(b1,b2,c1,c2);
        double cd=distance(c1,c2,d1,d2);
        double da=distance(d1,d2,a1,a2);
        if(ab==bc || ab==cd || ab==da){
            System.out.println("It is a rectangle");
        }
        else{
            System.out.println("It is not a rectangle");
        }
    }
    public static double distance(double x1, double x2,double y1,double y2){
        double r=Math.pow(Math.pow(y1-x1,2)+Math.pow(y2-x2, 2), 0.5);
        return r;


    }
    
}
