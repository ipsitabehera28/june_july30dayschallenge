import java.util.*;
class D30Q55 {
	double re,img;
	D30Q55(){
		re=0.0d; img=0.0d;
	}
	void data(double re,double img) {
		this.re=re;
		this.img=img;
	}
	public D30Q55 add(D30Q55 num1,D30Q55 num2) {
		D30Q55 num=new D30Q55();
		num.re=num1.re+num2.re;
		num.img=num1.img+num2.img;
		return num;
	}
	void display() {
		System.out.println(re+"+"+img+"i");
	}
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		D30Q55 num=new D30Q55();
		D30Q55 num1=new D30Q55();
		D30Q55 num2=new D30Q55();
		System.out.println("Enter the real part of first number: ");
		double r1=sc.nextDouble();
		System.out.println("Enter the imaginary part of first number: ");
		double im1=sc.nextDouble();
		num1.data(r1,im1);
		System.out.println("Enter the real part of first number: ");
		double r2=sc.nextDouble();
		System.out.println("Enter the imaginary part of first number: ");
		double im2=sc.nextDouble();
		num2.data(r2, im2);
		System.out.println("Your first complex number: "+r1+im1+"i   &  second complex number: "+r2+im2+"i");
		System.out.println("Sum = ");
		num.display();
	}
}
//notdone