import java.util.*;
public class D30Q81 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter a even array size : ");
		int n=sc.nextInt();
		int arr[]=new int[n];
		System.out.println("Enter Array element : ");
		for(int i=0;i<arr.length;i++) {
			arr[i]=sc.nextInt();
		}
		int result1=0,result2=0;
		for(int i=0;i<n/2;i++) {
			result1=result1+arr[i];
		}
		for(int i=n/2;i<n;i++) {
			result2=result2+arr[i];
		}
		int fres=Math.abs(result1-result2);
		System.out.println("To make the array balance we need to add " + fres);
	}
}