import java.util.*;

public class D30Q17 {
    public static void bill(int a[],int[] q) {
		int sum=0;
		for(int i=0;i<a.length;i++) {
			switch(a[i]) {
			case 1:
				sum=sum+(350*q[i]);
				break;
			case 2:
				sum=sum+(420*q[i]);
				break;
			case 3:
				sum=sum+(440*q[i]);
				break;
			case 4:
				sum=sum+(510*q[i]);
				break;
			case 5:
				sum=sum+(35*q[i]);
				break;
			case 6:
				sum=sum+(40*q[i]);
				break;
			}
		}
		System.out.println("Thanks for visiting here\n"
				+ "Bill Generated...\n"
				+ "Please pay Rs. " + sum + " through any mode of payment.");
	}
	
	public static void main(String[] args) {   
		Scanner sc=new Scanner(System.in);
		System.out.println("Price list of all items:\r\n"
				+ "	      1.Cheese Pizza : Rs. 350\r\n"
				+ "	      2.Chicken Burst Pizza :  Rs. 420\r\n"
				+ "	      3.Double Cheese Mixed Pizza: Rs. 440\r\n"
				+ "	      4.Mushroom Grilled Cheese Burst Pizza: Rs. 510\r\n"
				+ "	      5.Coke (300ml) : Rs. 35\r\n"
				+ "	      6.Mineral Water:  Rs. 40");
		System.out.println("Enter total number of items : ");
		int n =sc.nextInt();
		int a[]=new int[n];
		System.out.println("Enter the item no. those you want to take : ");
		for(int i=0;i<n;i++) {
			a[i]=sc.nextInt();
		}
		System.out.println("Enter the corresponding quantity: ");
		int q[]=new int[n];
		for(int i=0;i<n;i++) {
			q[i]=sc.nextInt();
		}
		bill(a,q);
	}
}
