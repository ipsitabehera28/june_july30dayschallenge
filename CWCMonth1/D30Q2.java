import java.util.*;
public class D30Q2 {
	static int cout;
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Fibonacci series upto which number: ");
        int n=sc.nextInt();
        int a[]=new int[n];        
        System.out.println("Sum of number of fibonacci series upto n is:  ");
        for(int i=0;i<n;i++){
        	//int cout=0;
            a[i]=fibo(i);
            cout=cout+a[i];
        }    
        System.out.println(cout);		
    }
    public static int fibo(int n) {
		if(n<=1) {
			return n;
		}
		else {
			return fibo(n-1)+fibo(n-2);
		}
	}
}
