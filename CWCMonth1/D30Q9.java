import java.math.BigInteger;

public class D30Q9 {
    public static void main(String[] args) {
        BigInteger j=new BigInteger("1");
        BigInteger a=new BigInteger("0");
        for(int i=1;i<=100;i++){
            j=j.multiply(BigInteger.valueOf(i));
        }
        System.out.println(j);
        BigInteger cout=BigInteger.ZERO;
        //int cout=0;
        while(j!=BigInteger.valueOf(0)){
            //BigInteger a=new BigInteger(j);
            a=a.add(j.mod(BigInteger.valueOf(10)));
        	j=j.divide(BigInteger.valueOf(10));
            //j=j/10;
            //cout++;
            //cout=cout.add(cout);
        }
        System.out.println("Number of digits are: "+a);
    }
}
