import java.util.*;
public class D30Q11 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the total number of paths you want to compare: ");
        int a=sc.nextInt();
        double b[]=new double[a];
        for(int i=0;i<a;i++){
        	double x1,y1,x2,y2;
            if((i+1)%10==1 && (i+1)%100!=11){
                System.out.println("Enter the constraints for "+ (i+1) + "st path: ");
                System.out.println("Enter x and y coordinates of the initial position: ");
                x1=sc.nextDouble();
                y1=sc.nextDouble();  
                System.out.println("Enter x and y coordinates of the final position: ");
                x2=sc.nextDouble();
                y2=sc.nextDouble();
            }
            else if((i+1)%10==2 && (i+1)%100!=12){
                System.out.println("Enter the constraints for "+ (i+1) + "ns path: ");
                System.out.println("Enter x and y coordinates of the initial position: ");
                x1=sc.nextDouble();
                y1=sc.nextDouble();  
                System.out.println("Enter x and y coordinates of the final position: ");
                x2=sc.nextDouble();
                y2=sc.nextDouble();
            }
            else if((i+1)%10==3 && (i+1)%100!=13){
                System.out.println("Enter the constraints for "+ (i+1) + "rd path: ");
                System.out.println("Enter x and y coordinates of the initial position: ");
                x1=sc.nextDouble();
                y1=sc.nextDouble();  
                System.out.println("Enter x and y coordinates of the final position: ");
                x2=sc.nextDouble();
                y2=sc.nextDouble();
            }
            else{
                System.out.println("Enter the constraints for "+ i + "th path: ");
                System.out.println("Enter x and y coordinates of the initial position: ");
                x1=sc.nextDouble();
                y1=sc.nextDouble();  
                System.out.println("Enter x and y coordinates of the final position: ");
                x2=sc.nextDouble();
                y2=sc.nextDouble();
            }
            double x=Math.pow(x2-x1, 2);
			double y=Math.pow(y2-y1, 2); 
            b[i]=Math.pow(x+y,0.5);       
        }
        for(int i=0;i<b.length;i++){
            System.out.printf("%.2f",b[i]);
            System.out.print(" Units Distance will be covered in path "+(i+1)+ ".");
            System.out.println();
        }
        double t=b[0];
        for(int i=0;i<b.length;i++){
            if(b[i]<t){
                t=b[i];
            }
        }
        int ind=0;
        for(int i=0;i<b.length;i++){
            if(b[i]==t){
                ind=i;
            }
        }
        System.out.println("You should go with path "+(ind+1)+".");
    }
}
