import java.util.*;
public class D30Q53 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter upto which term tou want to print: ");
        int n=sc.nextInt();
        for(int i=1;i<=n;i++){
            if(i==n) System.out.print((int)Math.pow(i,2)+","+(int)Math.pow(i,3));
            else System.out.print((int)Math.pow(i,2)+","+(int)Math.pow(i,3)+",");
        }
    }
}
