import java.util.*;
public class D30Q32 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("For nCm enter n then m: ");
        int n=sc.nextInt();
        int m=sc.nextInt();
        int nue=fact(n);
        int deno1=fact(n-m);
        int deno2=fact(m);
        int totdeno=deno1*deno2;
        double coe=nue/totdeno;
        System.out.println("Binominal coefficient is: "+coe);
    }
    public static int fact(int a){
        if (a==1) return 1;
        else return a*fact(a-1);
    }
}
