import java.util.*;
public class D30Q82 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter the length of array: ");
        int a=sc.nextInt();
        int b[]=new int[a];
        System.out.println("Enter the elements(0 and 1 only): ");
        for(int i=0;i<a;i++){
            b[i]=sc.nextInt();
        }
        System.out.println("Array is: ");
        for(int i=0;i<a;i++){
            System.out.print(b[i]+" ");
        }
        System.out.println();
        //main coding
        if(b[0]==1) {
        	first();
        }
        else {
            System.out.println("Transition point:");
        	second(b,a);
        }
    }
    public static void first() {
    	System.out.println("-1"+" there is no transition point.");   	
    }
    public static void second(int b[],int a) {
    	for(int i=1;i<a;i++) {
    		if(b[i]==0) {
    			continue;
    		}
    		else {
    			int r=i;
    			System.out.println(r);;
    		}
    	}
    }
}

