import java.util.*;
public class D30Q33 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter number of sections: ");
        int sec=sc.nextInt();
        for(int i=0;i<sec;i++){        
            System.out.println("Enter the number of elements: ");
            int num=sc.nextInt();
            int arr[]=new int[num];
            System.out.print("Enter the elements: ");
            for(int j=0;j<num;j++){
                arr[j]=sc.nextInt();
            }
            for(int j=0;j<num;j++){
                System.out.print(arr[j]+" ");
                //System.out.println(check(j,num));
                //check(arr,num);
            }
            check(arr,num);
            System.out.println();
            //System.out.println(check(arr,num));
            
        }
    }
    public static void check(int arr[],int num) {
    	int t=0;
    	System.out.print("  And after sorting: ");
    	for(int i=0;i<num;i++) {
    		for(int j=0;j<num-i-1;j++) {
    			if(arr[j]>arr[j+1]) {
        			t=arr[j];
        			arr[j]=arr[j+1];
        			arr[j+1]=t;
    		}		
    		} 		 		
    	}
    	//System.out.print(arr[i]+" ");
    	for(int i=0;i<num;i++) {
    		System.out.print(arr[i]+" ");
    	}
    	System.out.println();
    }
}
